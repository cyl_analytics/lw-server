#!/bin/bash

openssl genrsa -out key.pem 2048
openssl req -new -key key.pem -out certificate.csr -subj "/C=GB/ST=Yorks/L=York/O=MyCompany Ltd./OU=IT/CN=localhost"
openssl x509 -req -in certificate.csr -signkey key.pem -out certificate.pem
