{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Main where

import           Control.Concurrent                   (MVar, forkIO,
                                                       newEmptyMVar, putMVar,
                                                       takeMVar)
import           Control.Exception.Base               (bracket)
import           Data.Aeson                           (FromJSON, ToJSON, encode,
                                                       genericParseJSON)
import           Data.Aeson.Types                     (Options (..), camelTo,
                                                       defaultOptions)
import           Data.ByteString.Lazy                 (ByteString)
import           Data.Char                            (toUpper)
import           Data.List.Split                      (splitOn)
import           Data.Text                            (Text)
import           GHC.Generics                         (Generic)
import           Logger_Types                         (LogEntities (..),
                                                       LogLine, LogMessage,
                                                       LogSeverity,
                                                       read_LogEntities)
import           Network.HTTP.Types.Status            (status200)
import           Network.Wai                          (Application, Request,
                                                       lazyRequestBody,
                                                       responseLBS)
import           Network.Wai.Handler.Warp             (Port, defaultSettings,
                                                       setPort)
import           Network.Wai.Handler.WarpTLS          (TLSSettings (..),
                                                       defaultTlsSettings,
                                                       runTLS)
import           Network.Wai.Middleware.Gzip          (def, gzip)
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           System.Exit                          (die)
import           Thrift.Protocol.Binary               (BinaryProtocol (..))
import           Thrift.Transport.Wai                 (fromRequest)

import qualified Data.Yaml                            as Yaml

import           Network.AMQP
import           Options.Applicative


instance ToJSON LogSeverity
instance ToJSON LogLine
instance ToJSON LogMessage
instance ToJSON LogEntities


data LoggerHandler =
    LoggerHandler RMQ

data RMQ = RMQ (MVar RMQCommand)

data RMQCommand
    = RMQMessage ByteString
    | Stop (MVar ())

initRMQ ::RMQConfig -> IO RMQ
initRMQ rmqConfig = do
    m <- newEmptyMVar
    let rmq = RMQ m
    _ <- forkIO (rmqWriter rmqConfig rmq)
    return rmq

rmqWriter :: RMQConfig -> RMQ -> IO ()
rmqWriter RMQConfig{..} (RMQ m) = do
    conn <-
        openConnection'
            rmqHost
            (fromIntegral rmqPort)
            rmqVirtualHost
            rmqLoginName
            rmqLoginPassword
    ch <- openChannel conn
    loop conn ch
  where
    loop c ch = do
        cmd <- takeMVar m
        case cmd of
            RMQMessage msg -> do
                publishMsg
                    ch
                    ""
                    "log_queue"
                    (newMsg
                     { msgBody = msg
                     , msgDeliveryMode = Just NonPersistent
                     })
                loop c ch
            Stop s -> do
                closeConnection c
                putMVar s ()

writeMessage :: RMQ -> ByteString -> IO ()
writeMessage (RMQ m) bs = putMVar m (RMQMessage bs)

rmqStop :: RMQ -> IO ()
rmqStop (RMQ m) = do
        s <- newEmptyMVar
        putMVar m (Stop s)
        takeMVar s

newLoggerHandler :: RMQConfig -> IO LoggerHandler
newLoggerHandler rmqConfig = do
    rmq <- initRMQ rmqConfig
    return (LoggerHandler rmq)

delLoggerHandler :: LoggerHandler -> IO ()
delLoggerHandler (LoggerHandler rmq) = rmqStop rmq


application :: LoggerHandler -> Application
application (LoggerHandler rmq) req respond =
    bracket
        (fromRequestLogEntities req)
        (\le ->
              mapM_
                  (\e ->
                        writeMessage rmq (encode e))
                  (logEntities_entities le))
        (\_ ->
              respond $ responseLBS status200 [] "")


fromRequestLogEntities :: Request -> IO LogEntities
fromRequestLogEntities req = fromRequest req >>= read_LogEntities . BinaryProtocol


data LWServerCLOpts = LWServerCLOpts { config :: FilePath }


data LWServerConfig = LWServerConfig
    { configRmq :: RMQConfig
    , configApp :: AppConfig
    } deriving (Generic)

data RMQConfig = RMQConfig
    { rmqHost          :: String
    , rmqPort          :: Int
    , rmqVirtualHost   :: Text
    , rmqLoginName     :: Text
    , rmqLoginPassword :: Text
    } deriving (Generic)

data AppConfig = AppConfig
    { appPort     :: Port
    , appCertFile :: FilePath
    , appKeyFile  :: FilePath
    } deriving (Generic)

instance FromJSON AppConfig where
    parseJSON =
        genericParseJSON
            (defaultOptions
             { fieldLabelModifier = camelTo '_' . drop 3
             })

instance FromJSON RMQConfig where
    parseJSON =
        genericParseJSON
            (defaultOptions
             { fieldLabelModifier = camelTo '_' . drop 3
             })

instance FromJSON LWServerConfig where
    parseJSON =
        genericParseJSON
            (defaultOptions
             { fieldLabelModifier = camelTo '_' . drop 6
             })

underscoreToCamel :: String -> String
underscoreToCamel s = foldr (<>) "" (map capitalize (splitOn "_" s))

capitalize :: String -> String
capitalize [] = []
capitalize (h:t) = toUpper h : t

cliParser :: Parser LWServerCLOpts
cliParser =
    LWServerCLOpts <$>
    strOption
        (long "config" <> metavar "FilePath" <> help "Path to config file")

main :: IO ()
main = do
    clicfg <- execParser opts
    parsedConfig <- Yaml.decodeFile (config clicfg)
    case parsedConfig of
        Nothing -> die "Config is wrong"
        Just LWServerConfig{..} -> do
            handler <- newLoggerHandler configRmq
            print "Starting the server ..."
            runTLS
                (defaultTlsSettings
                 { certFile = appCertFile configApp
                 , keyFile = appKeyFile configApp
                 })
                (setPort (appPort configApp) defaultSettings) $
                logStdout $ gzip def $ application handler
            delLoggerHandler handler
  where
    opts =
        info
            (helper <*> cliParser)
            (fullDesc <> progDesc "Run log collector (server)" <>
             header "lw-server")
